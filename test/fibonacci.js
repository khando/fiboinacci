const chai = require('chai');
const fibonacci = require('../lib/fibonacci');

chai.should();

describe('fibonacci', function () {
  context('n = 0', function () {
    it('should return 0', function () {
      fibonacci(0).should.equal(0);
    });
  });

  context('n = 1', function () {
    it('should return 1', function () {
      fibonacci(1).should.equal(1);
    });
  });

  context('n = 2', function () {
    it('should return 1', function () {
      fibonacci(2).should.equal(1);
    });
  });

  context('n > 2', function () {
    it('should return right number', function () {
      fibonacci(5).should.equal(5);
      fibonacci(10).should.equal(55);
      fibonacci(15).should.equal(610);
      fibonacci(19).should.equal(4181);
      fibonacci(24).should.equal(46368);
    });
  });
});