function fibonacci(n) {
  let x1 = 1;
  let x2 = 1;
  if (n === 0) {
    return 0;
  }
  if (n === 1) {
    return 1;
  }
  if (n === 2) {
    return 1;
  }
  let x = 0;
  for (let i = 2; i < n; i += 1) {
    x = x1 + x2;
    x1 = x2;
    x2 = x;
  }
  return x;
}

module.exports = fibonacci;
